#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
サンプルプログラム
"""
import os
import imageio
import numpy as np
import matplotlib.pyplot as plt


def load_image_as_gray(filename):
    """ 入力画像をグレースケール画像として読み込む．
    入力画像はnumpy.arrayの2次元arrayとして読み込まれる．

    Inputs:
        filename (string): 入力画像のファイル名．
    Returns:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
    """
    assert os.path.exists(filename),\
        "%s does not exist." % filename

    I = np.array(imageio.imread(filename, as_gray=True),
                 dtype=float)

    return I


def process_1(I):
    """ 何かしらの処理をする関数．
    入力画像の各画素に対して一様分布U(-30, 30)から生成したノイズを加える処理が実装してある．
    入力画像Iと同じ型，shapeのnumpy.arrayを返す．

    Inputs:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
    Returns:
        I_mid (numpy.array): 処理結果．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
    """
    I_mid = I + np.random.randint(-30, 30, I.shape)

    return I_mid


def process_2(I, I_mid):
    """ 何かしらの処理をする関数．
    入力画像Iと中間出力画像I_midの差分画像を計算する処理が実装してある．
    入力画像I, I_midと同じ型，shapeのnumpy.arrayを返す．

    Inputs:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
        I_mid (numpy.array): 謎の中間出力画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
    Returns:
        I_result (numpy.array): 処理結果．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
    """

    I_result = I_mid - I

    return I_result


def show_results(I, I_mid, I_result):
    """ 結果を表示する．
    入力画像I，中間出力画像I_mid，結果画像I_resultを並べて表示する．

    Inputs:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array
        I_mid (numpy.array): 謎の中間出力画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
        I_result (numpy.array): 処理結果．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
    """
    fig, ax = plt.subplots(nrows=1, ncols=3)

    # show the input image
    ax[0].set_title('Input image')
    ax[0].imshow(I, cmap='gray')

    # show the intermidiate result
    ax[1].set_title('Intermediate result')
    ax[1].imshow(I_mid, cmap='gray')

    # show the result
    ax[2].set_title('Final result')
    ax[2].imshow(I_result, cmap='gray')


if __name__ == '__main__':
    filename = 'good.jpg'
    I = load_image_as_gray(filename)

    # 1. do something
    I_mid = process_1(I)

    # 2. do something
    I_result = process_2(I, I_mid)

    # 3. show the results
    show_results(I, I_mid, I_result)
