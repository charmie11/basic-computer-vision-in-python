# Basic Computer Vision in Python
このレポジトリはComputer Visionの基本的な問題をPythonで実装するプロジェクトのためのページです．

OpenCVなどが提供する関数を使うのではなく，NumPyなどの機能を駆使してComputer Visionの手法の実装を行います．

## 使い方
1. このレポジトリを自分の環境にダウンロードする
2. [各プロジェクト](./projects)を進める

## プロジェクト一覧
- [簡単な画像処理の実装方法](./projects/ImageProcessing)
- [Harrisのコーナー検出](./projects/HarrisCorner)
- [Homographyの計算](./projects/HomographyEstimation)
- Zhangの平面を使ったカメラ校正
- Structure from Motion
- Bundle Adjustment

## テンプレート
- [レポート (TeX)](./template/report)
- [プログラム (python)](./template/code)

### レポート
レポートはTeXで書きます．  
具体的には，以下の設定でコンパイルするように設計されています．
- コンパイラ: xelatex
- bibコンパイラ: biber

#### TeXのバージョン
以下の環境＋バージョンでの動作を確認しています．
- Ubuntu 16.04
  - XeTeX: 3.14159265-2.6-0.99998 (TeX Live 2017/Debian)
  - biber: 2.7

#### TexStudio (IDE)の設定
以下のようになります．
![TexStudio設定](./settings_texstudio.png "TexStudio設定")

### プログラム
プログラムはPythonで書きます．

テンプレートのプログラムを実行すると，以下のようなグラフが表示されます．
![テンプレートプログラムの処理結果](template_code_result.png "テンプレートプログラムの処理結果")

#### Pythonのバージョン
以下の環境＋バージョンでの動作を確認しています．
- Ubuntu 16.04
  - Python: 3.6.5
  - NumPy: 1.13.3
  - MatPlotLib: 2.2.2
  - SciPy: 1.1.0

## プロジェクトの進め方

1. [問題を理解](#問題を理解)
2. [手法を理解](#手法を理解)
3. [実装](#実装)

### 問題を理解
1. 入出力は何か理解する
2. 問題の難しさは何か理解する

### 手法を理解
1. 書籍，論文などを参考に，手法・アルゴリズムを理解する
2. 擬似コードを書く
3. 文章で説明する

### 実装
1. [main関数を実装](#main関数)する
  - 手順2.2で書いた擬似コードを基に実装する
  - main関数を実行すれば期待される(形式の)出力が得られるように実装する
2. main関数から呼ばれる全ての[サブ関数を宣言](#関数宣言)する
  - 入出力を定義する
  - コメントを書く
    - 関数の役割を説明する
    - 入出力の情報(型や意味)を書く
4. 各サブ関数の実装にかかる時間を予測する
5. [サブ関数を実装](#関数実装)する

#### main関数
main関数の実装例を以下に示す．
入力データから出力データを得る一連の処理を書き上げる．
```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-


if __name__ == '__main__':
    filename = 'good.jpg'
    I = load_image_as_gray(filename)

    # 1. compute gradient
    I_x = compute_gradient(I, 'x')
    I_y = compute_gradient(I, 'y')

    # 2. smooth the gradients
    sigma = 3.0
    I_x = smooth_image(I_x, sigma)
    I_y = smooth_image(I_y, sigma)

    # 3. compute corner response
    R = compute_corner_response(I_x, I_y)

    # 4. binarize the corner response
    tau = 3.0
    B = binarize_image(R, tau)

    # 5. find local maxima
    C = find_local_maxima(B)

    # 6. show the results
    show_results(I, C)
```

#### 関数宣言
関数の入出力を定義し，コメントに関数の役割及び入出力データの情報(型や意味)を書く．
main関数を実行できるように，ダミーの処理を書くのがポイント．  
以下の例では，全要素の値を0とする画像を合成している．
```python
def load_image_as_gray(filename):
    """ 入力画像をグレースケール画像として読み込む．
    入力画像はnumpy.arrayの2次元arrayとして読み込まれる．

    Inputs:
        filename (string): 入力画像のファイル名．
    Returns:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
    """

    I = np.zeros((300, 200))

    return I
```

#### 関数実装
関数の中身を実装する．  
assertもちゃんと書く．
```python
def load_image_as_gray(filename):
    """ 入力画像をグレースケール画像として読み込む．
    入力画像はnumpy.arrayの2次元arrayとして読み込まれる．

    Inputs:
        filename (string): 入力画像のファイル名．
    Returns:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
    """
    assert os.path.exists(filename),\
        "%s does not exist." % filename

    I = np.array(imageio.imread(filename, as_gray=True),
                 dtype=float)

    return I
```
