#!/usr/bin/env python
import os
import glob
import random
import imageio
import re
import numpy as np
import matplotlib.pyplot as plt


def stringSplitByNumbers(x):
    """ 文字列から数字を分離させる関数．
    stackoverflowで見つけたコードなので，どのような原理で動作しているのか謎．
    """
    r = re.compile('(\d+)')
    l = r.split(x)
    return [int(y) if y.isdigit() else y for y in l]


def imread_gray(filename):
    """ 画像をグレースケール画像として読み込む．

    Input:
        filename (str): 読み込む画像のファイル名．
    Returns:
        img (numpy.array): 読み込まれた画像．
            グレースケール画像なので，サイズ(Height, Width)のnumpy.array型オブジェクトになる．
    """
    assert os.path.exists(filename), '%s does not exist!' % filename

    img = np.array(imageio.imread(filename))
    if len(img.shape) == 3:
        img = 0.299*img[:, :, 0] + 0.587*img[:, :, 1] + 0.114*img[:, :, 2]

    return img


def load_data_CMU_Motion(dirname_root='./CMU_Motion', dataname='hotel', indices=None):
    """ CMU Motionデータセットから画像ペア，点群ペアを取得する．

    画像データは下記のURLから取得できる．
        http://www.cs.cmu.edu/afs/cs/project/vision/vasc/idb/images/motion/
    Input:
        dirname_root (str): データセットのルートディレクトリ．
            デフォルト値は'./CMU_Motion'．
        dataname (str): データセットの名前．'hotel'か'house'のどちらか
            デフォルト値は'hotel'．
        indices (list of integer):
            デフォルト値は'./CMU_Motion'．
    Returns:
        Images (a list of 2 numpy.array): 画像ペア．
            Images[0]，Images[1]はそれぞれソース画像，ターゲット画像を表すnumpy.array型オブジェクト．
        Points (a list of 2 numpy.array): 点群データのペア．
            Points[0]，Points[1]はそれぞれソース点群，ターゲット点群を表すnumpy.array型オブジェクト．
            両者共サイズ(N, 2)のnumpy.array型オブジェクト．
        Indices (a list of 2 integer): データのインデックス．
            Indices[0]，Indices[1]はそれぞれソース画像とターゲット画像の番号を表す．
    """
    assert dataname in ['hotel', 'house'], \
        'The dataname must be either hotel or house.'
    dirname = os.path.join(dirname_root, dataname)
    assert os.path.exists(dirname), \
        '%s does not exist!' % dirname

    filenames_image = glob.glob(os.path.join(dirname, '*.png'))
    filenames_image.sort(key=stringSplitByNumbers)

    filenames_point = glob.glob(os.path.join(dirname, '*.point'))
    filenames_point.sort(key=stringSplitByNumbers)

    assert len(filenames_image) == len(filenames_point),\
        'The number of image and point files must be same.'
    assert len(filenames_image) > 1,\
        'The number of image and point files must be greater than 1.'

    num_data = len(filenames_image)
    if indices is None:
        indices = random.choices(list(range(num_data)), k=2)
    indices.sort()
    assert type(indices) == list and len(indices) == 2,\
        'The indices must be a list of 2 integers.'
    assert indices[0] < num_data and indices[1] < num_data,\
        'The indices must be less than num_data'

    img_src = imread_gray(filenames_image[indices[0]])
    img_dst = imread_gray(filenames_image[indices[1]])
    pt_src = np.loadtxt(filenames_point[indices[0]], delimiter=',')
    pt_dst = np.loadtxt(filenames_point[indices[1]], delimiter=',')

    assert pt_src.shape == pt_dst.shape,\
        'Two pointstes must be in same dimension and have same number of poitns.'

    return (img_src, img_dst), (pt_src, pt_dst), indices


def convert_cartesian_to_homogeneous(pts_cart):
    """ 点群の座標を直交座標系(Cartesian coordinate)から同次座標系(Homogeneous coordinate)へ変換

    pts_cartとpts_homoの関係は以下の式で表せる．
        pts_homo[i, 0] = pts_cart[i, 0]
        ...
        pts_homo[i, D-1] = pts_cart[i, D-1]
        pts_homo[i, D] = 1.0

    Args:
        pts_cart (numpy.array): 直交座標系で表された点群．
            D次元空間中のN個の点をサイズ（N, D)のNumpy.arrayオブジェクトで表す．
    Returns:
        pts_homo (numpy.array): 同次座標系で表された点群．
            D次元空間中のN個の点をサイズ（N, D+1)のNumpy.arrayオブジェクトで表す．
    """

    num_points = pts_cart.shape[0]
    ones = np.ones((num_points, 1))
    pts_homo = np.concatenate((pts_cart, ones), axis=1)

    return pts_homo


def convert_homogeneous_to_cartesian(pts_homo):
    """ 点群の座標を同次座標系(Homogeneous coordinate)から直交座標系(Cartesian coordinate)へ変換

    pts_homoとpts_cartの関係は以下の式で表せる．
        pts_cart[i, 0] = pts_homo[i, 0] / pts_homo[i, -1]
        ...
        pts_cart[i, D-1] = pts_homo[i, D-1] / pts_homo[i, -1]

    Args:
        pts_homo (numpy.array): 同次座標系で表された点群．
            D次元空間中のN個の点をサイズ（N, D+1)のNumpy.arrayオブジェクトで表す．
    Returns:
        pts_cart (numpy.array): 直交座標系で表された点群．
            D次元空間中のN個の点をサイズ（N, D)のNumpy.arrayオブジェクトで表す．
    """

    pts_cart = np.divide(pts_homo[:, :-1], pts_homo[:, -1].reshape(-1, 1))

    return pts_cart


def precondition_points(pts):
    """ HomogrpahyやF行列の計算の際に，数値解析の安定化のために行う処理．

    事前補正によって，点群の重心は原点に位置し，xy方向のバラつき(標準偏差)が1.0になる．

    まずはじめに，このような事前補正を行うためのアフィン変換行列Tを計算する．
    次に，計算したアフィン変換を元の点群ptsにかける．
    アフィン変換によって得られる点群pts_homoは同次座標系の点群であるため，直交座標系に戻す．

    アフィン変換行列Tは
        T = [[s 0 -s*mu_x],
             [0 s -s*mu_y],
             [0 0       1]]
    となる．
    ここで，sはptsのx,y座標の標準偏差の逆数，mu_x, mu_yはptsのx,y座標の平均値を表す．

    Args:
        pts (numpy.array): 元の点群．
            N個の2次元点をサイズ（N, 2)のNumpy.arrayオブジェクトで表す．
    Returns:
        pts_cond (numpy.array): 事前補正された点群．
            N個の2次元点をサイズ（N, 2)のNumpy.arrayオブジェクトで表す．
        T (numpy.array): 事前補正を表すアフィン行列．
            サイズ(3, 3)のNumpy.arrayオブジェクトで表す．
    """
    assert len(pts.shape) == 2 and pts.shape[1] == 2, 'The points must be in 2D.'

    # TODO 事前補正を表すアフィン行列Tを計算してください．
    T = np.eye(3)

    # TODO アフィン行列Tを使って点群データptsを自薦補正してください．
    pts_cond = np.zeros(pts.shape)

    return pts_cond, T


def project_points(pts, H):
    """ Homographyを使った点群の投影．
    Args:
        pts (numpy.array): 投影前の2次元点群．
            N個の2次元点をサイズ（N, 2)のNumpy.arrayオブジェクトで表す．
        H (numpy.array): Homography.
            サイズ（3, 3)のNumpy.arrayオブジェクトで表す．
    Returns:
        pts_proj (numpy.array): 投影後の2次元点群．
            N個の2次元点をサイズ（N, 2)のNumpy.arrayオブジェクトで表す．
    """
    assert len(pts.shape) == 2 and pts.shape[1] == 2, 'The points must be in 2D.'
    assert H.shape == (3, 3), 'Homography must be a 3x3 matrix.'

    pts_homo = convert_cartesian_to_homogeneous(pts)
    pts_proj = convert_homogeneous_to_cartesian(pts_homo.dot(H.T))

    return pts_proj


def compute_reprojection_error(pts_src, pts_dst, H):
    """ Homographyを使った再投影誤差を計算．
            error = || H*pts_src - pts_dst ||_2

    Args:
        pts_src (numpy.array): A set of 2d source points.
            The shape of the array is (N, 2).
        pts_dst (numpy.array): A set of 2d destination points.
            The shape of the array is (N, 2).
        H (numpy.array): A homography.
            The shape of the array is (3, 3).

    Returns:
        error (scalar): The computed reprojection error.

    """

    assert pts_src.shape == pts_dst.shape, 'The pointset pair must be same shape'
    assert pts_src.shape[1] == 2, 'The points must be in 2D.'
    assert H.shape == (3, 3), 'Homography must be a 3x3 matrix.'

    pt_proj = project_points(pts_src, H)

    error_proj = np.linalg.norm(pts_dst - pt_proj, axis=1)
    error = np.mean(error_proj)

    return error


def build_linear_system_Axb(pts_src, pts_dst):
    """ Homography計算のための線形方程式のA,bを構築．

    以下の式と同意となる線形方程式Ax=bを構築する．
        pts_dst = H * pts_src
        [u_n,   [[H_11 H_12 H_13],   [x_n,
         v_n, =  [H_21 H_22 H_23], *  y_n, (1)
           1]    [H_31 H_32    1]]      1]

    Args:
        pts_src (numpy.array): A set of 2d source points.
            The shape of the array is (N, 2).
        pts_dst (numpy.array): A set of 2d destination points.
            The shape of the array is (N, 2).
    Returns:
        A (numpy.array): The linear system of Ax=b.
            The shape of the matrix is (2*N, 8).
        b (numpy.array): The column vector of Ax=b.
            The shape of the vector is (2*N).
    """
    num_points = pts_src.shape[0]
    A = np.zeros((2*num_points, 8))
    b = np.zeros(2*num_points)

    # TODO A, bの各要素に値を代入してください．

    return A, b


def solve_homography_Axb(A, b):
    """ 線形方程式Ax=bを解き，Homographyを求める．

    Args:
        A (numpy.array): The linear system of Ax=b.
            The shape of the matrix is (2*N, 8).
        b (numpy.array): The column vector of Ax=b.
            The shape of the vector is (2*N).
    Returns:
        H_cond (numpy.array): The computed homogrpahy.
            Note that the estimated homography has pre-conditioning effect.
            The shape of the matrix is (3, 3).
    """
    assert A.shape[1] == 8, 'The shape of the matrix must be (2*N, 8).'

    # TODO Ax=bを解き，H_condを計算してください．
    H_cond = np.eye(3)

    return H_cond


def deprecondition_homography(H_cond, T_x, T_u):
    """ 計算で得られたHomographyから事前補正の影響を取り除く．

    事前補正を考慮しないHomography Hと考慮するHomogrpahy H_condを使うと，点群pts_src, pts_dstの関係は以下のように表される．
        U = H * X ... (1)
        U_cond  = H_cond * X_cond ... (2)
        U_cond = T_u * U ... (3)
        X_cond = T_x * X ... (4)
    (3), (4)式を(2)式に代入すると
        T_u * U = H_cond * T_x * X ... (5)
    となる．
    (1)，(5)式を比較すると，HとH_condの関係は
        H = inv(T_u) * H_cond * T_x
    となることが分かる．

    Args:
        H_cond (numpy.array): The computed homogrpahy.
            Note that the estimated homography has pre-conditioning effect.
            The shape of the matrix is (3, 3).
        T_x (numpy.array): Pre-conditioning matrix for source pointsets.
            The shape of the matrix is (3, 3).
        T_u (numpy.array): Pre-conditioning matrix for destination pointsets.
            The shape of the matrix is (3, 3).
    Returns:
        H (numpy.array): The homogrpahy, whose pre-conditioning effect is canceled.
            The shape of the matrix is (3, 3).
    """
    assert H_cond.shape == (3, 3), 'Homography must be a 3x3 matrix.'
    assert T_x.shape == (3, 3), 'Affine transformation matrix must be 3x3.'
    assert T_u.shape == (3, 3), 'Affine transformation matrix must be 3x3.'

    # TODO H_cond, T_x, T_uからHを計算する．
    H = np.eye(3)

    return H


def plot_homography(imgs, pts, H):
    """ 推定したHomographyの精度を視覚的に確認するための関数．

    横に並んだ2つのプロットエリアに各画像と投影前後の点をプロットする．
    左のプロットエリアではimgs[0]の上にpts[0]が+として，H^{-1}*pts[1]がxとしてプロットされる．
    右のプロットエリアではimgs[1]の上にH*pts[0]が+として，pts[1]がxとしてプロットされる．

    Args:
        imgs (a list of 2 numpy.array): 2枚の画像．
            imgs[0]がソース画像，imgs[1]がターゲット画像．
            それぞれがnumpy.arrayオブジェクト．
        pts (a list of 2 numpy.array):
            pts[0]がソース点群，pts[1]がターゲット点群．
            それぞれサイズ(N, 2)のnumpy.arrayオブジェクト．
        H (a list of 2 numpy.array): Homography.
            サイズ(3, 3)のnumpy.arrayオブジェクト．
    """
    assert len(imgs) == 2, "The number of images must be 2."
    assert len(pts) == 2, "The number of pointsets must be 2."
    assert pts[0].shape[0] == pts[1].shape[0], "The size of pointsets must be same."
    assert H.shape == (3, 3), "Homography must be a 3x3 matrix."

    fig, axes = plt.subplots(1, 2)

    num_points = pts[0].shape[0]
    colors = [tuple(np.random.rand(3).tolist()) for m in range(num_points)]
    markers = ['+', 'x']
    for n in range(2):
        # make a plot
        axes[n].set_title('%s[%d]' % (dataname, indices[n]))
        axes[n].axis('off')
        axes[n].imshow(imgs[n], cmap='gray')

    # draw raw points and reprojected points
    for n in range(2):
        if n == 1:
            H = np.linalg.inv(H)
        pt_proj = project_points(pts[n], H)

        for m in range(num_points):
            # draw raw points
            axes[n].scatter(pts[n][m, 0],
                            pts[n][m, 1],
                            c=colors[m],
                            marker=markers[n])
            # draw reprojected points
            axes[1-n].scatter(pt_proj[m, 0],
                              pt_proj[m, 1],
                              c=colors[m],
                              marker=markers[n])


if __name__ == '__main__':
    np.set_printoptions(precision=1)
    dirname_root = './CMU_Motion'
    dataname = 'house'

    imgs, pts, indices = load_data_CMU_Motion(dirname_root, dataname)
    X = pts[0]
    U = pts[1]

    # Step 1: 点群データの事前補正
    X_cond, T_x = precondition_points(X)
    U_cond, T_u = precondition_points(U)

    # Step 2: 線形システム(Ax=b)を作成
    A, b = build_linear_system_Axb(X_cond, U_cond)

    # Step 3: Ax=bを解きX_cond, U_cond間のHomographyを推定
    H_cond = solve_homography_Axb(A, b)

    # Step 4: X, U間のHomographyを推定
    H = deprecondition_homography(H_cond, T_x, T_u)

    # Step 5: Homographyの推定誤差を計算
    error = compute_reprojection_error(X, U, H)
    print('The reprojection error is', error)

    # Step 6: 結果のプロット
    plot_homography(imgs, pts, H)
