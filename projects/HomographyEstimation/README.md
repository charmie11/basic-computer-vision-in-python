# Homography estimation with Python
今回はHomogrpahyの推定法を実装します．  
詳細は[Multiple View Geometryの4章](http://www.robots.ox.ac.uk/~vgg/hzbook/)を読んでください．
Homography推定法を提案している論文が見つからなかったため，Multiple View Geometryの4章を参照するよう書きましたが，原著論文を見つけたらぜひご一報ください．
```
@inbook{BookMultipleView,
  author       = {Richard Hartley and Andrew Zisserman},
  title        = {Multiple View Geometry in Computer Vision},
  chapter      = {4 Estimation - 2D Projective Transformations},
  pages        = {87-131},
  publisher    = {Cambridge University Press},
  year         = {2003},
  edition      = {2}
}

```

## データセット
今回は[CMU Motionデータセット](http://www.cs.cmu.edu/afs/cs/project/vision/vasc/idb/images/motion)の内，特徴点の座標データを入手可能なhotel, houseデータセットを使用します．

### 点群ファイル
hotel, houseディレクトリ内に保存されている各pointファイルには，以下のように画像上の特徴点30個の座標データが保存されています．  
i行目のデータにはi番目の特徴点のx,y座標がカンマで区切られて保存されています．
```
171.000000,81.000000
274.000000,118.000000
...
494.000000,375.000000
```

### 画像ファイル
[codeディレクトリ](./code)に保存されているスクリプトファイルを使用して，画像ファイルをダウンロードしてください．  
Windows使用者はdownload_CMU_Motion.bat，Unix/Linux使用者はdownload_CMU_Motion.shを実行すると./code/CMU_Motionディレクトリ内にhotel，houseデータセットがダウンロードされます．

## テンプレート
- [レポート (TeX)](./report)
- [プログラム (python)](./code)

### レポート (TeX)
今回は理論の説明も兼ねて一部の数式と文章を記載しています．
虫食いになっている式や説明を書き加えて，文章を完成させてください．

### プログラム (python)
今回もmain関数とサブ関数の定義，コメントは書いてあります．  
main関数から呼ばれる以下のサブ関数の実装をしてください．  
load_data_CMU_Motion()，compute_reprojection_error()，plot_homography()は実装済みなので，参考にしてください．  
各サブ関数内で# TODOのコメントが書いてある部分を完成させてください．
- precondition_points()
- build_linear_system_Axb()
- solve_homography_Axb()
- deprecondition_homography()

```
if __name__ == '__main__':
    np.set_printoptions(precision=1)
    dirname_root = './CMU_Motion'
    dataname = 'house'

    imgs, pts, indices = load_data_CMU_Motion(dirname_root, dataname)
    X = pts[0]
    U = pts[1]

    # Step 1: 点群データの事前補正
    X_cond, T_x = precondition_points(X)
    U_cond, T_u = precondition_points(U)

    # Step 2: 線形システム(Ax=b)を作成
    A, b = build_linear_system_Axb(X_cond, U_cond)

    # Step 3: Ax=bを解きX_cond, U_cond間のHomographyを推定
    H_cond = solve_homography_Axb(A, b)

    # Step 4: X, U間のHomographyを推定
    H = deprecondition_homography(H_cond, T_x, T_u)

    # Step 5: Homographyの推定誤差を計算
    error = compute_reprojection_error(X, U, H)
    print('The reprojection error is', error)

    # Step 6: 結果のプロット
    plot_homography(imgs, pts, H)
```

## 結果
正しく実装できると以下のようなグラフが出力されるようになります．
![hotelデータセットの結果](result_hotel.png "hotelデータセットの結果")
![houseデータセットの結果](result_house.png "houseデータセットの結果")
