#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Harrisのコーナー検出器のNumPyの実装．
詳細は下記の論文を参照．
http://www.bmva.org/bmvc/1988/avc-88-023.pdf
@INPROCEEDINGS{Harris88acombined,
    author = {Chris Harris and Mike Stephens},
    title = {A combined corner and edge detector},
    booktitle = {Fourth Alvey Vision Conference},
    year = {1988},
    pages = {147--151}
}
"""
import os
import imageio
import numpy as np
from skimage.feature.peak import _prominent_peaks
import matplotlib.pyplot as plt


def load_image_as_gray(filename):
    """ 入力画像をグレースケール画像として読み込む．
    入力画像はnumpy.arrayの2次元arrayとして読み込まれる．

    Inputs:
        filename (string): 入力画像のファイル名．
    Returns:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
    """
    assert os.path.exists(filename),\
        "%s does not exist." % filename

    I = np.array(imageio.imread(filename, as_gray=True),
                 dtype=float)

    return I


def compute_gradient(I, direction):
    """ 画像の1次微分を計算する．
    第2引数directionによって，微分の方向を指定する．

    Inputs:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
        direction (string): 微分を計算する方向．
            direction = 'x'なら，x方向の1次微分を計算する．
            direction = 'y'なら，y方向の1次微分を計算する．
    Returns:
        I_grad (numpy.array): 計算した1次微分を表す画像．
            入力画像Iと同じshapeを持つnumpy.array型オブジェクト．
    """
    assert len(I.shape) == 2,\
        "The input image I must be a gray-scale image."
    assert direction in ['x', 'y'],\
        "The gradient direction must be either of 'x' or 'y'"

    I_grad = I
    return I_grad


def smooth_image(I, sigma):
    """ 画像を平滑化する．
    以下のURLを参考にする．
    https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.ndimage.filters.gaussian_filter.html

    Inputs:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
        sigma (float): 平滑化のためのGaussianカーネルの標準偏差．
    Returns:
        I_smooth (numpy.array): 平滑化した画像．
            入力画像Iと同じshapeを持つnumpy.array型オブジェクト．
    """
    assert sigma > 0,\
        "The specified sigma for Gaussian kernel must be positive number."

    I_smooth = I

    return I_smooth


def compute_corner_response(I_x, I_y):
    """ 画像のコーナーレスポンスを計算する．

    Inputs:
        I_x (numpy.array): x方向の1次微分画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
        I_y (numpy.array): x方向の1次微分画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
    Returns:
        R (numpy.array): コーナーレスポンス画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
            R[y, x]は画素(x,y)のコーナーレスポンスを表す．
    """
    assert I_x.shape == I_y.shape,\
        "The shape of I_x and I_y must be same."

    R = I_x + I_y

    return R


def binarize_image(I, tau):
    """ 画像を2値化する．

    Inputs:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
        tau (float): 2値化の閾値．
    Returns:
        I_bi (numpy.array): 2値画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
    """
    assert tau > 0,\
        "The specified threshold value must be positive number."

    I_bi = I

    return I_bi


def non_maximum_suppression(I):
    """ 画像のnon-maximum suppressionを探索する．

    Inputs:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array
    Returns:
        I_nms (numpy.array): 検出したnon-maximum suppressionを表す2値画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
            I_nms[x, y] = Trueなら，画素(x，y)はコーナーである．
            I_nms[x, y] = Falseなら，画素(x，y)はコーナーではない．
    """
    I_nms = np.zeros(I.shape, dtype=bool)

    value, x, y = _prominent_peaks(I,
                                   min_xdistance=1,
                                   min_ydistance=1)

    num_peaks = len(value)
    for n in range(num_peaks):
        I_nms[y[n], x[n]] = True

    return I_nms


def show_results(I, R, B, C):
    """ コーナー検出の結果を表示する．
    入力画像Iとコーナー検出結果画像Cを並べて表示する．

    Inputs:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array
        R (numpy.array): 中間出力．コーナーレスポンス画像．
            Height x Widthのshapeを持つnumpy.array
        B (numpy.array): 中間出力．2値化したコーナーレスポンス画像．
            Height x Widthのshapeを持つnumpy.array
        C (numpy.array): 出力コーナー検出結果画像．
            Height x Widthのshapeを持つnumpy.array
    """
    fig, ax = plt.subplots(nrows=1, ncols=4)

    # show the input image
    ax[0].set_title('Input image')
    ax[0].imshow(I, cmap='gray')

    # show the corner response
    ax[1].set_title('Corner response')
    ax[1].imshow(R, cmap=plt.get_cmap('hot'))

    # show the binarized corner response
    ax[2].set_title('Binarized corner response')
    ax[2].imshow(B, cmap='gray')

    # show the detected corners
    x, y = np.where(C == True)
    ax[3].set_title('Detected corners')
    ax[3].imshow(I)
    ax[3].scatter(y.tolist(), x.tolist())


if __name__ == '__main__':
    filename = 'test.png'
    I = load_image_as_gray(filename)

    # 1. compute gradient
    I_x = compute_gradient(I, 'x')
    I_y = compute_gradient(I, 'y')

    # 2. smooth the gradients
    sigma = 3.0
    I_x = smooth_image(I_x, sigma)
    I_y = smooth_image(I_y, sigma)

    # 3. compute corner response
    R = compute_corner_response(I_x, I_y)

    # 4. binarize the corner response
    tau = 3.0
    B = binarize_image(R, tau)

    # 5. compute non-maximum_suppression
    C = non_maximum_suppression(B)

    # 6. show the results
    show_results(I, R, B, C)
