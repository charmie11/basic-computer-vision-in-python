\documentclass[12pt,a4]{article}

\label{misc:preamble}
\usepackage{xltxtra}
\setmainfont{IPAPMincho}
\setsansfont{IPAPGothic}
\setmonofont{IPAGothic}
\XeTeXlinebreaklocale "ja"

\usepackage{hyperref}

% マージン(ページ上下左右のスペース)の設定
\usepackage[a4paper]{geometry}
\geometry{
	left=20mm,
	right=20mm,
	top=20mm
}

\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{bm}
\DeclareMathOperator{\Tr}{Tr}

\usepackage{graphicx}

\usepackage{algorithm}
\usepackage{algpseudocode}

\usepackage{todonotes}

\usepackage[backend=biber, style=numeric]{biblatex}
\addbibresource{HarrisCorner.bib}

\makeatletter
\renewcommand{\figurename}{図}
\renewcommand{\tablename}{表}
\renewcommand{\ALG@name}{アルゴリズム}
\renewcommand{\refname}{参考文献}
\makeatother

\label{misc:main}
\title{Harrisのコーナー検出器}
\author{著者}
\date{\today}

\begin{document}
\maketitle

今回はHarrisとStephensが提案したコーナー検出法\cite{Harris1988}を実装します．  

\par
このテンプレートは数式など最低限の情報のみを記述するに留めているので，数式や変数を使っ説明をする文章は自身で書き上げてください．

\section{問題}
\label{sec:problem}
\begin{itemize}
\item 入力: 画像$\bm{I} \in \mathbb{R}^{H \times W}$
\item 出力: 検出したコーナーの位置$\mathcal{C} = \{\bm{c}_1, \ldots, \bm{c}_{N_c}\}, \bm{c}_i = (c_{i, x}, c_{i, y})^T$
\end{itemize}
$W$と$H$はそれぞれ画像の横幅，縦幅を表す．

\begin{figure}[tb]
\centering
\missingfigure{コーナー検出の例}
\caption{コーナー検出の例．}
\label{fig:problem}
\end{figure}

\section{手法}
\label{sec:method}
\begin{algorithm}[tb]
	\caption{Harrisのコーナー検出}
	\label{alg:sample}
	\textbf{入力:} 画像$\bm{I}$\\
	\textbf{出力:} コーナー検出画像$\bm{I}_{\textrm{corner}}$
	\begin{algorithmic}[1]
		\State 画像$\bm{I}$の1次微分$\bm{I}_x, \bm{I}_y$を計算
		\State 1次微分画像をGaussianカーネルで平滑化
		\State コーナーレスポンス$\bm{R}$を計算
		\State コーナーレスポンス$\bm{R}$を2値化
		\State 2値化したコーナーレスポンス$\bm{R}_{\textrm{bi}}$のNon-Maximum Suppressionをコーナーとして検出
	\end{algorithmic}
\end{algorithm}

\subsection{勾配の計算}
\label{ssec:method_gradient}
\begin{align}
\bm{I}_x &= \frac{\partial \bm{I}}{\partial x}
	\label{eq:gradient_x} \\
\bm{I}_y &= \frac{\partial \bm{I}}{\partial y}
	\label{eq:gradient_y}
\end{align}

\subsection{微分画像の平滑化}
\label{ssec:method_smooth}
\begin{align}
\bm{I}_{\textrm{smooth}} &= \mathrm{smooth}(\bm{I}_{\textrm{raw}}, \sigma)
	\label{eq:smooth_func} \\
&= \bm{I}_{\textrm{raw}} \otimes \bm{K}_\sigma
	\label{eq:smooth} \\
\bm{K}_\sigma(x, y) &= \frac{1}{2\pi\sigma^2}\exp\left(-\frac{x^2+y^2}{2\sigma^2}\right)
	\label{eq:gaussian_kernel}
\end{align}
ここで，$\otimes$は畳み込み演算子，$\sigma$はGaussianフィルタの標準偏差を表す．

\subsection{コーナーレスポンスの計算}
\label{ssec:method_corner_response}
\begin{align}
\bm{I}(x + d_x, y + d_y) = 
\bm{I}(x, y) + &d_x\frac{\partial \bm{I}(x, y)}{\partial x} + d_y\frac{\partial \bm{I}(x, y)}{\partial y} + \nonumber\\
&\frac{1}{2!}d_x^2\frac{\partial^2 \bm{I}(x, y)}{\partial x^2} + \frac{1}{2!}d_y^2\frac{\partial^2 \bm{I}(x, y)}{\partial y^2} + \nonumber\\
&\ldots + \nonumber\\
&\frac{1}{n!}d_x^n\frac{\partial^n \bm{I}(x, y)}{\partial x^n}
+ \frac{1}{n!}d_y^n\frac{\partial^n \bm{I}(x, y)}{\partial y^n} + \ldots \label{eq:derivative}\\
\approx \bm{I}(x, y) + &d_x\frac{\partial \bm{I}(x, y)}{\partial x} + d_y\frac{\partial \bm{I}(x, y)}{\partial y} 
\label{eq:derivative_approx}
\end{align}

\begin{align}
\bm{I}_x = d_x\frac{\partial \bm{I}(x, y)}{\partial x} \\
\bm{I}_y = d_y\frac{\partial \bm{I}(x, y)}{\partial y} 
\end{align}

\begin{align}
&\sum \left(I(x+d_x, y+d_y) - I(x, y)\right)^2 \nonumber\\
&\approx \sum \left(I(x, y) + d_x\bm{I}_x(x, y) + d_y\bm{I}_y(x, y) - I(x, y)\right)^2 \nonumber\\
&= \sum \left(d_x\bm{I}_x(x, y) + d_y\bm{I}_y(x, y)\right)^2 \nonumber\\
&= \sum \left(d_x^2\bm{I}_x^2(x, y) + 2d_x d_y \bm{I}_x(x, y) \bm{I}_y(x, y) + d_y^2\bm{I}_y^2(x, y)\right)^2 \nonumber\\
&= \sum 
\begin{bmatrix}
d_x & d_y
\end{bmatrix}
\begin{bmatrix}
\bm{I}_x^2(x, y) & \bm{I}_x(x, y) \bm{I}_y(x, y) \nonumber\\
\bm{I}_x(x, y) \bm{I}_y(x, y) & \bm{I}_y^2(x, y)
\end{bmatrix}
\begin{bmatrix}
d_x \\
d_y
\end{bmatrix} \\
&= 
\begin{bmatrix}
d_x & d_y
\end{bmatrix}
\left(
\sum 
\begin{bmatrix}
\bm{I}_x^2(x, y) & \bm{I}_x(x, y) \bm{I}_y(x, y) \\
\bm{I}_x(x, y) \bm{I}_y(x, y) & \bm{I}_y^2(x, y)
\end{bmatrix}
\right)
\begin{bmatrix}
d_x \\
d_y
\end{bmatrix}
\end{align}

\begin{align}
\bm{M} = \sum_{(x, y) \in \mathcal{N}} w(x, y) 
\begin{bmatrix}
\bm{I}_x^2(x, y) & \bm{I}_x(x, y) \bm{I}_y(x, y) \\
\bm{I}_x(x, y) \bm{I}_y(x, y) & \bm{I}_y^2(x, y)
\end{bmatrix}
\end{align}
\begin{align}
\bm{R} = \det \bm{M} - k \left(\Tr \bm{M} \right)^2
\end{align}

\subsection{Non-Maximum Suppressionの計算}
\label{ssec:method_nms}
ゲロゲロに複雑なので，全てを理解できなくても構いません．


\section{実験結果}
\label{sec:results}
実装が終了したら，色々なデータを使って実験をします．
漠然と実験をするのではなく，意図を持った実験を行いしょう．

\printbibliography

\end{document}