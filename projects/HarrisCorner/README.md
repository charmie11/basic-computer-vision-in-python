# Harris corner detection in Python
今回はHarrisとStephensが提案したコーナー検出法を実装します．  
詳細は[原著論文](http://www.bmva.org/bmvc/1988/avc-88-023.pdf)を読んでください．
```
@INPROCEEDINGS{Harris88acombined,
    author = {Chris Harris and Mike Stephens},
    title = {A combined corner and edge detector},
    booktitle = {Fourth Alvey Vision Conference},
    year = {1988},
    pages = {147--151}
}
```

## テンプレート
これが初めてのプロジェクトであるため，今回のレポート・プログラムは数式やmain関数，サブ関数のコメントなどがある程度書いてあります．  
今後のプロジェクトの参考にしてください．
- [レポート (TeX)](./report)
- [プログラム (python)](./code)


## ヒント
各処理の実装で使えそうな関数を以下に示します．
ある程度自分で調べた上で解決策がわからない場合はヒントとして読んでください．
- 画像(numpy.array)の1次微分: [numpy.gradient](https://docs.scipy.org/doc/numpy/reference/generated/numpy.gradient.html)
- 画像(numpy.array)の平滑化: [scipy.ndimage.filters.gaussian_filter](https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.gaussian_filter.html)
- 行列のdeterminant(行列式): [np.linalg.det](https://docs.scipy.org/doc/numpy/reference/generated/numpy.linalg.det.html)
- 行列のtrace(跡): [np.trace](https://docs.scipy.org/doc/numpy/reference/generated/numpy.trace.html)

## 結果
codeディレクトリに保存されているtest.png，chessboard.pngを使うと以下のようなグラフが出力されるように実装を進めてください．
![test.pngの結果](result_test.png "test.pngの結果")
![chessboard.pngの結果](result_chessboard.png "chessboard.pngの結果")
