#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Yuji Oyamada
"""
import os
import imageio
import numpy as np
import matplotlib.pyplot as plt


def load_image_as_gray(filename):
    """ 入力画像をグレースケール画像として読み込む．
    入力画像はnumpy.arrayの2次元arrayとして読み込まれる．

    Inputs:
        filename (string): 入力画像のファイル名．
    Returns:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
    """
    assert os.path.exists(filename),\
        "%s does not exist." % filename

    I = np.array(imageio.imread(filename, as_gray=True),
                 dtype=float)

    return I


def access_to_elements_in_loop():
    shape = (16, 16)
    Y, X = shape

    M_y = np.zeros(shape)
    for y in range(Y):
        M_y[y, :] = 16*y

    M_x = np.zeros(shape)
    for x in range(X):
        M_x[:, x] = 16*x

    M_yx = np.zeros(shape)
    for y in range(Y):
        for x in range(X):
            M_yx[y, x] = y*16+x

    M_xy = np.zeros(shape)
    for y in range(Y):
        for x in range(X):
            M_xy[y, x] = y+x*16

    fig, ax = plt.subplots(nrows=2, ncols=2)
    ax[0][0].imshow(M_y, cmap='gray')
    ax[0][0].set_title('M[y,x] = 16*y')
    ax[0][1].imshow(M_x, cmap='gray')
    ax[0][1].set_title('M[y,x] = 16*x')
    ax[1][0].imshow(M_yx)
    ax[1][0].set_title('M[y,x] = 16*y+x')
    ax[1][1].imshow(M_xy, cmap='inferno')
    ax[1][1].set_title('M[y,x] = y+16*x')

    imageio.imwrite('loop_y.png', M_y)
    imageio.imwrite('loop_x.png', M_x)
    imageio.imwrite('loop_y_x.png', M_yx)
    imageio.imwrite('loop_x_y.png', M_xy)

    return M_x, M_y, M_xy, M_yx


def access_to_elements():
    shape = (16, 16)
    Y, X = shape

    M_y = 16*np.tile(np.arange(Y), (Y, 1)).transpose()
    M_x = 16*np.tile(np.arange(X), (Y, 1))
    M_yx = np.arange(X*Y).reshape(shape)
    M_xy = np.arange(X*Y).reshape(shape, order='F')

    return M_x, M_y, M_xy, M_yx


def binarize_in_loop(I, tau):
    """ 画像を2値化する．

    I_bi[y, x] = 255 if I[y, x] >= tau
               = 0   otherwise

    Inputs:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
    Returns:
        I_bi (numpy.array): 2値画像．
            入力画像Iと同じshapeを持つnumpy.array型オブジェクト．
    """

    I_bi = np.zeros(I.shape)
    Y, X = I.shape
    for y in range(Y):
        for x in range(X):
            if I[y, x] < tau:
                I_bi[y, x] = 0
            else:
                I_bi[y, x] = 255

    fig, ax = plt.subplots(ncols=2)
    ax[0].imshow(I, cmap='gray')
    ax[0].set_title('Input image')
    ax[1].imshow(I_bi, cmap='gray')
    ax[1].set_title('Binarized image (tau = %d)' % tau)

    return I_bi


def binarize(I, tau):

    I_bi = np.zeros(I.shape)
    I_bi[I >= tau] = 255

    return I_bi


def compute_gradient_in_loop(I):
    """ 画像の1次微分を計算する．

    I_x[y, x] = I[y, x] - I[y, x-1]
    I_y[y, x] = I[y, x] - I[y-1, x]

    Inputs:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
    Returns:
        I_x (numpy.array): 計算したx軸方向に関する1次微分を表す画像．
            入力画像Iと同じshapeを持つnumpy.array型オブジェクト．
        I_y (numpy.array): 計算したy軸方向に関する1次微分を表す画像．
            入力画像Iと同じshapeを持つnumpy.array型オブジェクト．
    """
    assert len(I.shape) == 2,\
        "The input image I must be a gray-scale image."

    Y, X = I.shape

    I_x = np.zeros(I.shape, dtype=float)
    for y in range(Y):
        for x in range(X):
            if x > 0:
                I_x[y, x] = I[y, x] - I[y, x-1]

    I_y = np.zeros(I.shape, dtype=float)
    for y in range(Y):
        if y > 0:
            for x in range(X):
                I_y[y, x] = I[y, x] - I[y-1, x]

    fig, ax = plt.subplots(ncols=3)
    ax[0].imshow(I, cmap='gray')
    ax[0].set_title('Input image')
    ax[1].imshow(I_x, cmap='gray')
    ax[1].set_title('Gradient wrt x')
    ax[2].imshow(I_y, cmap='gray')
    ax[2].set_title('Gradient wrt y')

    return I_x, I_y


def compute_gradient(I):
    """ 画像の1次微分を計算する．

    I_x[y, x] = I[y, x] - I[y, x-1]
    I_y[y, x] = I[y, x] - I[y-1, x]

    Inputs:
        I (numpy.array): 入力グレースケール画像．
            Height x Widthのshapeを持つnumpy.array型オブジェクト．
    Returns:
        I_x (numpy.array): 計算したx軸方向に関する1次微分を表す画像．
            入力画像Iと同じshapeを持つnumpy.array型オブジェクト．
        I_y (numpy.array): 計算したy軸方向に関する1次微分を表す画像．
            入力画像Iと同じshapeを持つnumpy.array型オブジェクト．
    """
    assert len(I.shape) == 2,\
        "The input image I must be a gray-scale image."

    Y, X = I.shape

    I_x = np.zeros(I.shape, dtype=float)
    I_x[:, 1:] = I[:, 1:] - I[:, :-1]

    I_y = np.zeros(I.shape, dtype=float)
    I_y[1:, :] = I[1:, :] - I[:-1, :]

    return I_x, I_y


def compute_diff(I1, I2):
    assert I1.shape == I2.shape,\
        'The two images must be same shape.'

    return np.sum(np.abs(I1 - I2))


if __name__ == '__main__':
    """ トピック1: 画像の各要素へのアクセス
    access_to_elements_in_loop()ではfor loopで画像の各要素の値を計算
    access_to_elements()では全く同じ処理を1行で実現
    """
    M_x, M_y, M_xy, M_yx = access_to_elements_in_loop()
    M_x2, M_y2, M_xy2, M_yx2 = access_to_elements()
    # 2つの関数で得られた結果が同じか否かチェック
    print('diff M_x', compute_diff(M_x, M_x2))
    print('diff M_y', compute_diff(M_y, M_y2))
    print('diff M_xy', compute_diff(M_xy, M_xy2))
    print('diff M_yx', compute_diff(M_yx, M_yx2))

    filename = 'lena.png'
    I = load_image_as_gray(filename)

    """ トピック2: 画像の2値化
    binarize_in_loop()ではfor loopで画像を2値化
    binarize()では全く同じ処理を1行で実現
    """
    tau = 64.0
    I_bi = binarize_in_loop(I, tau)
    I_bi2 = binarize(I, tau)
    # 2つの関数で得られた結果が同じか否かチェック
    print('diff I_bi', compute_diff(I_bi, I_bi2))

    """ トピック3: 画像の勾配の計算
    compute_gradient_in_loop()ではfor loopで画像のx方向，y方向の微分を計算
    compute_gradient()では全く同じ処理を1行で実現
    """
    I_x, I_y = compute_gradient_in_loop(I)
    I_x2, I_y2 = compute_gradient(I)
    # 2つの関数で得られた結果が同じか否かチェック
    print('diff I_x', compute_diff(I_x, I_x2))
    print('diff I_y', compute_diff(I_y, I_y2))
